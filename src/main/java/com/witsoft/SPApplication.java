package com.witsoft;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


/**
 * Hello world!
 *
 */
@Slf4j
@SpringBootApplication
@MapperScan({"com.witsoft.**.dao"})
public class SPApplication
{
    @Value("${spring.profiles.active:default}")
    private String active;

    @Bean
    public void printActive(){
        log.info(">>>>>>>>>>IOT-SP当前运行环境：{}<<<<<<<<<<", active);
    }


    public static void main(String[] args){
        SpringApplication.run(SPApplication.class, args);
        log.info(">>>>>>>>>>IOT-SP服务启动成功<<<<<<<<<<");
    }
}
