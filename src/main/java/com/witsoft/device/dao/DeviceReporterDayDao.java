package com.witsoft.device.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.witsoft.device.entity.DeviceReporterDay;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface DeviceReporterDayDao extends BaseMapper<DeviceReporterDay> {


    /**
     * @desc 获取某设备昨天的生产总数
     * @date 2022.01.13
     * @param deviceId
     * @return
     */
    @Select("select COALESCE(sum(number_of_products),0) as total from device_day_reporter where to_char(count_date, 'yyyy-MM-dd') = #{date} and device_id = #{deviceId}")
    Long getLastDayProductTotal(@Param("deviceId") String deviceId, @Param("date") String date);


    /**
     * @desc 获取某个设备的某个月份的性能稼动率 2021.12.03
     * @param deviceId
     * @param date
     * @return
     */
    @Select("select COALESCE(sum(performance_grain_move_rate),0) as total from device_day_reporter where to_char(count_date, 'yyyy-MM') = #{date} and device_id = #{deviceId}")
    Double getSumPerformanceGrainRateMonthById(@Param("deviceId")String deviceId, @Param("date") String date);


    /**
     * @desc 获取某个设备的某个月份的时间稼动率 2021.12.03
     * @param deviceId
     * @param date
     * @return
     */
    @Select("select COALESCE(sum(time_grain_move_rate),0) as total from device_day_reporter where to_char(count_date, 'yyyy-MM') = #{date} and device_id = #{deviceId}")
    Double getSumTimeGrainRateMonthById(@Param("deviceId")String deviceId, @Param("date") String date);
}
