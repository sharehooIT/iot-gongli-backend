package com.witsoft.device.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.model.DeviceTotalInfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Title:时序数据Mapper接口
 * @Description:
 * @Author:zhansh
 * @Date:2021/10/20 17:01
 */
public interface DeviceDao extends BaseMapper<DeviceEntity>{


    /**
     * @desc 设备运行状态饼图
     * @return
     */
    @Select("SELECT status, count(status) FROM device group by status")
    List<DeviceTotalInfo> getDeviceTotalInfo();

    @Select("select id,created_time as createdTime,type,name,alias,location,model_number as modelNumber,serial_number as serialNumber,kpi_cala_period as kpiCalaPeriod,device_meter as deviceMeter, \n"
            +"plan_start_time as planStartTime,plan_end_time as planEndTime,COALESCE(good_count, 0) as goodCount,COALESCE(bad_count,0) as badCount,COALESCE(total_count,0) as totalCount,ideal_run_rate as idealRunRate,sort,status \n"
            +"from device order by created_time asc")
    List<DeviceEntity> getAllList();

    @Select("select id,created_time as createdTime,type,name,alias,location,model_number as modelNumber,serial_number as serialNumber,kpi_cala_period as kpiCalaPeriod,device_meter as deviceMeter, \n"
            +"plan_start_time as planStartTime,plan_end_time as planEndTime,COALESCE(good_count,0) as goodCount,COALESCE(bad_count,0) as badCount,COALESCE(total_count,0) as totalCount,ideal_run_rate as idealRunRate,sort,status \n"
            +"from device where id=#{id}")
    DeviceEntity getInfo(String i);

    @Select("select id,created_time as createdTime,type,name,alias,location,model_number as modelNumber,serial_number as serialNumber,kpi_cala_period as kpiCalaPeriod,device_meter as deviceMeter, \n"
            +"plan_start_time as planStartTime,plan_end_time as planEndTime,good_count as goodCount,bad_count as badCount,total_count as totalCount,ideal_run_rate as idealRunRate,sort,status \n"
            +"from device where name=#{name}")
    DeviceEntity getDeviceByName(String name);

}
