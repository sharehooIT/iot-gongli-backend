package com.witsoft.device.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.witsoft.device.entity.DeviceMetricsLogEntity;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * @Title:时序数据Mapper接口
 * @Description:
 * @Author:zhansh
 * @Date:2021/10/20 17:01
 */
public interface DeviceMetricsLogDao extends BaseMapper<DeviceMetricsLogEntity>{

    /*
     * 根据设备Id取指定日期的设备指标数据
     */
    @Select("select id,device_id as deviceId,availability,performance,quality,oee,create_time as createTime,good_count as goodCount,bad_count as badCount,total_count as totalCount,type \n"
            +"from device_metrics_log where device_id=#{deviceId} and to_char(create_time,'yyyy-MM-dd')=to_char(#{date},'yyyy-MM-dd') and type = #{type} order by create_time asc ")
    public List<DeviceMetricsLogEntity> getMetricsLogList(String deviceId, Date date,String type);
}
