package com.witsoft.device.model;


import lombok.Data;

/**
 * @desc 设备指标信息
 */
@Data
public class DeviceQuota {

    //设备合格率
    private String quantity = "0";

    //性能开动率
    private String performance = "0";
    //时间开动率
    private String availability = "0";

    private String oee = "0";

    //稼动率
    private String grainMoveRate = "0";

    private String deviceName;

    private String serialNumber;
}
