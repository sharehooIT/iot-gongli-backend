package com.witsoft.device.model;


import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;

@Data
public class DeviceTotalInfo {

    private Integer status;
    private Integer count;

    @TableField(exist = false)
    private Integer total;

    @TableField(exist = false)
    private List<EchartVO> data;

    private Integer runningNum;
    private Integer errorNum;

    //稼动率
    private String grainMoveRate = "0";

    public DeviceTotalInfo(Integer status, Integer count) {
        this.status = status;
        this.count = count;
    }


    public DeviceTotalInfo(Integer total, List<EchartVO> data) {
        this.total = total;
        this.data = data;
    }
}
