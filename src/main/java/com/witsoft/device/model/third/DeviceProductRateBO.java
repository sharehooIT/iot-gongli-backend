package com.witsoft.device.model.third;

import lombok.Data;

/**
 * @desc mes api获取今日不合格数
 */
@Data
public class DeviceProductRateBO {

    private String acCode;

    private String acName;

    //设备id
    private String equipId;
    //不良产品数
    private Long failQuantity;
    //缺陷产品数
    private Long defectQuantity;
    //待处理数
    private Long todoQuantity;
}
