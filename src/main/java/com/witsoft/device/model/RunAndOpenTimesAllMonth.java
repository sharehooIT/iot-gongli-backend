package com.witsoft.device.model;


import lombok.Data;

@Data
public class RunAndOpenTimesAllMonth {

    //运行总时间
    private Long runningTimes;
    //开机总时间
    private Long openTimes;

    //时间
    private String date;
}
