package com.witsoft.device.model;


import lombok.Data;

import java.util.*;


/**
 * @desc 设备当日实时数据统计指标
 */
@Data
public class DeviceRealTimeSpent {

    private String deviceId;

    private String status;

    private Long totalSpent;

    //与当前时间实时计算出的结果
    private Double realTimeSpent;

    private Date startTime;


    /**
     * @desc 去重
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj){
        DeviceRealTimeSpent spent = (DeviceRealTimeSpent) obj;
        return deviceId.equals(spent.deviceId);
    }

    @Override
    public int hashCode(){
        return this.deviceId.hashCode();
    }


    public static void main(String args[]){
        DeviceRealTimeSpent spent = new DeviceRealTimeSpent();
        spent.setDeviceId("5");

        DeviceRealTimeSpent spent1 = new DeviceRealTimeSpent();
        spent1.setDeviceId("5");
        DeviceRealTimeSpent spent2 = new DeviceRealTimeSpent();
        spent2.setDeviceId("6");

        List<DeviceRealTimeSpent> dada = new ArrayList<>();
        dada.add(spent);
        dada.add(spent1);
        dada.add(spent2);

        Set<DeviceRealTimeSpent> spents = new HashSet<>(dada);

        System.out.println(spents);
    }
}
