package com.witsoft.device.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class DeviceMetricsLogInfo{

    @JsonProperty("id")
    private String   id;

    @JsonProperty("deviceId")
    private String   deviceId;

    @JsonProperty("availability")
    private String availability;

    @JsonProperty("performance")
    private String performance;

    @JsonProperty("quality")
    private String quality;

    @TableField("OEE")
    @JsonProperty("oee")
    private String oee;

    @JsonProperty("createTime")
    private Date   createTime;

    @JsonProperty("goodCount")
    private Long   goodCount;

    @JsonProperty("badCount")
    private Long   badCount;

    @JsonProperty("totalCont")
    private Long   totalCont;

    @JsonProperty("type")
    private String type;

}
