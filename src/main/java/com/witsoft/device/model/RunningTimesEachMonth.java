package com.witsoft.device.model;


import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @desc 所有月份的运行时间
 */
@Data
public class RunningTimesEachMonth {

    @TableField("sum")
    private Double total;

    @TableField
    private String date;
}
