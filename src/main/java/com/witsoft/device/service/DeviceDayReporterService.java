package com.witsoft.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.witsoft.device.entity.DeviceReporterDay;
import org.apache.ibatis.annotations.Param;

public interface DeviceDayReporterService extends IService<DeviceReporterDay> {


    /**
     * @desc 获取某个设备的某个月份的性能稼动率 2021.12.03
     * @param deviceId
     * @param date
     * @return
     */
    Double getSumPerformanceGrainRateMonthById(@Param("deviceId")String deviceId, @Param("date") String date);


    /**
     * @desc 获取某个设备的某个月份的时间稼动率 2021.12.03
     * @param deviceId
     * @param date
     * @return
     */
    Double getSumTimeGrainRateMonthById(@Param("deviceId")String deviceId, @Param("date") String date);


    /**
     * @desc 获取某设备昨天的生产总数
     * @param deviceId
     * @return
     */
    Long getLastDayProductTotal(@Param("deviceId") String deviceId);
}
