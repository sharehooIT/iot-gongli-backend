package com.witsoft.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.entity.DeviceReporterMonth;

import java.util.List;
import java.util.Map;

public interface DeviceReporterService extends IService<DeviceReporterMonth> {


    /**
     * @desc 获取所有设备的当天的平均时间稼动率
     * @return
     */
    Double getAllDeviceAverageTimeGrainRateByDay();


    /**
     * @desc 获取当月指定设备开机时间
     * @return
     */
    Long getDeviceSumOpeningMonth(String deviceId);


    /**
     * @desc 获取当月指定设备运行时间
     * @return
     */
    Long getDeviceSumRunningMonth(String deviceId);

    /**
     * @desc 获取当月所有的设备开机时间
     * @return
     */
    Long getAllDeviceSumOpeningMonth();


    /**
     * @desc 获取所有设备的当月的设备运行时间
     * @return
     */
    Long getAllDeviceSumRunningMonth();


    /**@date 2021.12.07
     * @desc 获取所有设备当月的时间稼动率- 重构：所有设备当月的时间稼动率 = sum(单个设备总运行时间 / 单个设备总开机时间) /设备总数
     * @return
     */
    Map<String, Object> getAllDeviceMonthGrainMoveRateV2();


    /**@date 2021.12.07
     * @desc 获取所有设备当月的性能稼动率- 重构：所有设备当月的时间稼动率 = sum(单个设备当月节拍数 * 报工时长 / 当月单个设备总开机时间) /设备总数
     * @return
     */
    Map<String, Object> getAllDeviceMonthPerformanceGrainMoveRateV2();


    /**
     * @desc 获取所有设备当月的时间稼动率 = 所有设备总运行时长 / 所有设备总的开机时长
     * @return
     */
    @Deprecated
    Map<String, Object> getAllDeviceMonthGrainMoveRate();


    /**
     * @desc 获取所有设备当月的性能稼动率=  sum（当月每个设备节拍数*报工数）/sum（当月每个设备总的运行时长）
     * @return
     */
    @Deprecated
    Map<String, Object> getAllDeviceMonthPerformanceGrainMoveRate();

    public String getDeviceCodes(List<DeviceEntity> list);
}
