package com.witsoft.device.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.witsoft.device.dao.DeviceDao;
import com.witsoft.device.dao.DeviceMetricsLogDao;
import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.entity.DeviceMetricsLogEntity;
import com.witsoft.device.service.DeviceMetricsLogService;
import com.witsoft.device.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DeviceMetricsLogServiceImpl extends ServiceImpl<DeviceMetricsLogDao, DeviceMetricsLogEntity> implements DeviceMetricsLogService{

    @Resource
    private DeviceMetricsLogDao deviceMetricsLogDao;
    @Override
    public List<DeviceMetricsLogEntity> getMetricsLogList(String deviceId, Date date,String type){
        List<DeviceMetricsLogEntity> list = deviceMetricsLogDao.getMetricsLogList(deviceId,date,type);
        return list;
    }
}
