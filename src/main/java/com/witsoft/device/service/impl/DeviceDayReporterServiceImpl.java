package com.witsoft.device.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.witsoft.device.dao.DeviceReporterDayDao;
import com.witsoft.device.entity.DeviceReporterDay;
import com.witsoft.device.service.DeviceDayReporterService;
import com.witsoft.device.utils.DateUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Date;


@Service
public class DeviceDayReporterServiceImpl extends ServiceImpl<DeviceReporterDayDao, DeviceReporterDay> implements DeviceDayReporterService {

    @Resource
    private DeviceReporterDayDao reporterDayDao;


    @Override
    public Long getLastDayProductTotal(String deviceId) {

        Date lastDay = DateUtil.getLastDay();
        Long lastDayProductTotal = reporterDayDao.getLastDayProductTotal(deviceId, DateUtil.sdf_d.format(lastDay));
        return lastDayProductTotal;
    }

    @Override
    public Double getSumPerformanceGrainRateMonthById(String deviceId, String date) {
        Double rateMonthById = reporterDayDao.getSumPerformanceGrainRateMonthById(deviceId, date);
        if(ObjectUtils.isEmpty(rateMonthById)){
            return 0.0;
        }

        //除以天数
        Integer currentMonthNumber = DateUtil.getCurrentMonthNumber();
        Integer daysOfMonth = DateUtil.getDaysOfMonth(currentMonthNumber);

        Double rate = rateMonthById / daysOfMonth;
        return rate;
    }

    @Override
    public Double getSumTimeGrainRateMonthById(String deviceId, String date) {

        Double rateMonthById = reporterDayDao.getSumTimeGrainRateMonthById(deviceId, date);
        if(ObjectUtils.isEmpty(rateMonthById)){
            return 0.0;
        }

        //除以天数
        Integer currentMonthNumber = DateUtil.getCurrentMonthNumber();
        Integer daysOfMonth = DateUtil.getDaysOfMonth(currentMonthNumber);

        Double rate = rateMonthById / daysOfMonth;
        return rate;
    }
}
