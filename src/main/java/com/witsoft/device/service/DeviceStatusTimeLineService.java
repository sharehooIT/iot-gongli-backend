package com.witsoft.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.witsoft.device.entity.DeviceStatusTimeLineEntity;
import com.witsoft.device.model.DeviceRealTimeSpent;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DeviceStatusTimeLineService extends IService<DeviceStatusTimeLineEntity>{

    /**
     * @desc 2022.04.07 获取当天实时开机时间计算
     */
    Long getRealOpeningTimeCurrentDay(String deviceId);

    /**
     * @desc 根据状态码获取前一天的设备的最近一条记录
     * feature(2021.12.20): 修复获取前一天设备状态记录异常的bug（实际获取的却是当天的）
     */
    DeviceStatusTimeLineEntity getLastOneInfoByStatusYesterday(@Param("deviceId")String deviceId, @Param("status")String status);


    /**
     * 取某一天的某设备的运行时间总数(单位：秒)
     */
    Long getSumRunningTimeByDayTime(@Param("deviceId") String deviceId, @Param("date") String date);


    /**
     * 取某一天的某设备的开机时间总数(单位：秒)
     */
    Long getSumOpenningTimeByDayTime(@Param("deviceId") String deviceId, @Param("date") String date);


    /**
     * @desc 过滤重复设备的最新记录，只保留最新的一条
     * @param data
     * @return
     */
    Map<String, DeviceRealTimeSpent> distinct(List<DeviceRealTimeSpent> data);


    void reportStatusNoSplitFlow(String obj);

    void reportStatusSplitFlow(String obj);

    List<DeviceStatusTimeLineEntity> getListByDeviceId(String deviceId);

    DeviceStatusTimeLineEntity getLastOneInfoByDeviceId(String deviceId);


    /**
     * @desc 获取当日的运行时长
     * @param deviceId
     * @return
     */
    int getSumRunningTimeDay(String deviceId);


    /**
     * @desc 获取当日的开机时长
     * @param deviceId
     * @return
     */
    int getSumOpeningTimeDay(String deviceId);


    /**
     * @desc 获取所有设备当日的运行时长：秒
     * @return
     */
    Long getSumAllRunningTimeDay();


    /**
     * @desc 获取所有设备当日的开机时长：秒
     * @return
     */
    Long getSumAllOpeningTimeDay();


    //获取上个月的运行总时长
    Long getSumRunningTimeLastMonth(String deviceId);

    //获取上个月的开机总时长
    Long getSumOpeningLastMonth(String deviceId);


    /**
     * @desc 获取所有设备当日的开机时长
     * @param deviceId
     * @return
     */
    int getSumTime(String deviceId);


    /**
     * @desc 获取当设备状态为Running，且total_spent为null的设备 ---- 解决如果当前设备一直是运行或无停机状态，无法统计有效的运行时长  fixed:2021.11.10
     */
    Long getSumAllRunningTimeDayOfTotalSpentIsNull();


    /**
     * @desc 获取当设备状态为Turnning，且total_spent为null的设备
     */
    Long getSumAllOpeningTimeDayOfTotalSpentIsNull();


    /**
     * @desc 根据状态码获取当天的设备的最近一条记录
     * @author miki
     */
    DeviceStatusTimeLineEntity getLastOneInfoByStatus(String deviceId, String status);
}
