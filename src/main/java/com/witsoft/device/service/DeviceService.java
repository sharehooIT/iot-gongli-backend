package com.witsoft.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.model.DeviceQuotaListInfo;
import com.witsoft.device.model.DeviceTotalInfo;
import com.witsoft.device.model.bo.DeviceQuotaBO;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DeviceService extends IService<DeviceEntity>{

    List<DeviceEntity> getAllList();

    /**
     * @desc 性能开动率计算公式，产品节拍数 * 报工数 / 总的运行时间
     * @param deviceCode
     * @param start
     * @param end
     * @param type : day 或month
     */
    Map<String, Double> getBeatMultiplyWorkReportOfDay(String deviceCode, Date start, Date end, String type);

    Map getBeatMultiplyWorkReportOfYear(String deviceCodes);


    /**
     * @desc 计算所有设备的指标oee，性能开动率，时间开动率
     * @return
     */
    DeviceQuotaListInfo getAverageDeviceTarget();

    DeviceQuotaBO getAverageDeviceTargetByDeviceId(String deviceId);

    DeviceEntity getInfo(String id);

    DeviceEntity getDeviceByName(String name);

    List<DeviceTotalInfo> getDeviceTotalInfo();
}
