package com.witsoft.device.enums;

public enum DeviceStatusEnum {

    MACHINE_OFFONLIE(0, "关机"),
    MACHINE_ONLIE(1, "开机");

    private Integer code;
    private String msg;


    DeviceStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public String getCodeStr(){
        return code.toString();
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
