package com.witsoft.device.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



@Slf4j
public class DateUtil {

    public final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
    public final static SimpleDateFormat sdf_y = new SimpleDateFormat("yyyy");
    public final static SimpleDateFormat sdf_d = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat sdf_s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



    /**
     * @desc 计算起始查询时间, 从当前时间往前推足够12个月份
     * @return
     */
    public static Date getLast12MonthsDateFromNow(){

        String startTime = null;
        //计算起始查询时间, 从当前时间往前推足够12个月份
        Integer currentMonthNumber = DateUtil.getCurrentMonthNumber();
        Integer lastYearNumber = DateUtil.getLastYearNumber();

        try{
            if(currentMonthNumber < 12){
                currentMonthNumber += 1;
                if(currentMonthNumber < 10){
                    startTime = lastYearNumber +"-0"+ currentMonthNumber +"-01 00:00:00";
                }else{
                    startTime = lastYearNumber +"-"+ currentMonthNumber +"-01 00:00:00";
                }

                return sdf_s.parse(startTime);
            }
        }catch (Exception e){
            log.error("获取日期格式化异常..{}", e.getMessage());
        }
        return null;
    }


    /**
     * @desc 获取当前月份的整数
     * @return
     */
    public static Integer getCurrentMonthNumber(){
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int monthValue = localDate.getMonthValue();

        return monthValue;
    }



    /**
     * @desc 获取当前年份的整数
     * @return
     */
    public static Integer getCurrentYearNumber(){
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int yearValue = localDate.getYear();

        return yearValue;
    }


    /**
     * @desc 获取去年份的整数
     * @return
     */
    public static Integer getLastYearNumber(){
        Integer currentYearNumber = getCurrentYearNumber();

        return currentYearNumber - 1;
    }



    public static Integer getDaysOfMonth(int month){
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.MONTH, month);
        //获取某月最大的一天
        int lastDay = 0;
        if(month == 2){
            lastDay = calendar.getLeastMaximum(Calendar.DAY_OF_MONTH);
        }else {
            lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        return lastDay;
    }


    /**
     * @desc 获取某天零点，零分，零秒的
     * @return
     */
    public static Date getCurrentZero(Date date){
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }


    /**
     * @desc 获取昨天的日期
     */
    public static Date getLastDay(){
        Instant instant = LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant();

        return Date.from(instant);
    }


    /**
     * @desc 获取上个月的月份日期
     * @return
     */
    public static Date getLastMonth(){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        //设置为上个月份
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);

        return calendar.getTime();
    }


    public static Long getLeftSeconds(Date start, Date end){

        long time = start.getTime();
        long time1 = end.getTime();

        Long second = (time1 - time) / 1000;

        return second;
    }


    /**
     * @desc 获取每个月的第一天
     * @param month
     * @return
     */
    public static String getFirstDayOfMonth(int month){

        Calendar calendar = Calendar.getInstance();
        //设置月份
        calendar.set(Calendar.MONTH, month - 1);
        //获取某月最小天数
        int firstDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最小天数
        calendar.set(Calendar.DAY_OF_MONTH, firstDay);
        //格式化日期
        String first = sdf_d.format(calendar.getTime()) + " 00:00:00";

        return first;
    }

    public static Date getFirstDayStampOfMonth(int month){
        try {
            return sdf_d.parse(getFirstDayOfMonth(month));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * @desc 获取每个月的第一天的时间戳
     * @param month
     * @return
     */
    public static Long getFirstDayTimeStampOfMonth(int month) throws ParseException {

        Calendar calendar = Calendar.getInstance();
        //设置月份
        calendar.set(Calendar.MONTH, month - 1);
        //获取某月最小天数
        int firstDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最小天数
        calendar.set(Calendar.DAY_OF_MONTH, firstDay);
        //格式化日期
        String first = sdf_d.format(calendar.getTime()) + " 00:00:00";

        long time = sdf_s.parse(first).getTime();
        return time;
    }


    /**
     * @desc 获取每个月的最后一天的时间戳
     * @param month
     * @return
     */
    public static Long getLastDayTimeStampOfMonth(int month) throws ParseException {

        Calendar calendar = Calendar.getInstance();
        //设置月份
        calendar.set(Calendar.MONTH, month - 1);
        //获取某月最大一天
        int lastDay = 0;
        //2月份的平年，瑞年天数
        if(month == 2){
            lastDay = calendar.getLeastMaximum(Calendar.DAY_OF_MONTH);
        }else {
            lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        //设置日历中月份的最小天数
        calendar.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        String last = sdf_d.format(calendar.getTime()) + " 23:59:59";

        long time = sdf_s.parse(last).getTime();
        return time;
    }


    /**
     * @desc 获取每个月的最后一天
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int month){

        Calendar calendar = Calendar.getInstance();
        //设置月份
        calendar.set(Calendar.MONTH, month - 1);
        //获取某月最大一天
        int lastDay = 0;
        //2月份的平年，瑞年天数
        if(month == 2){
            lastDay = calendar.getLeastMaximum(Calendar.DAY_OF_MONTH);
        }else {
            lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        //设置日历中月份的最大天数
        calendar.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        String last = sdf_d.format(calendar.getTime()) + " 23:59:59";

        return last;
    }


    public static void main(String args[]) throws ParseException {

        Long leftSeconds = getLeftSeconds(new Date(), new Date());
        System.out.println(leftSeconds);

        Long dd = 1l;
        System.out.println(dd > 0);

        List<Integer> integers = Arrays.asList(1, 2, 3);

        System.out.println(sdf.format(new Date()));
        for (Integer d: integers) {
            if(d == 2){
                System.out.println(2);
                continue;
            }
            System.out.println("####");
        }

        Date firstDayStampOfMonth = getFirstDayStampOfMonth(1);
        String lastDayOfMonth = getLastDayOfMonth(2);
        int i = Integer.parseInt("02");
        System.out.println(firstDayStampOfMonth +"###" + lastDayOfMonth + "##" + i);
    }

    /**
     * @desc 获取昨天现在的时间
     * @return
     */
    public static Date getLastDayOfMonth() {

        Instant instant = LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant();

        return Date.from(instant);
    }
}
