package com.witsoft.device.utils;

import java.text.DecimalFormat;

public class NumberUtil {

    public final static DecimalFormat df = new DecimalFormat("#");
    public final static DecimalFormat df0 = new DecimalFormat("#.0");
    public final static DecimalFormat df00 = new DecimalFormat("#.00");
}
