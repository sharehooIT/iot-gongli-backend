package com.witsoft.device.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

@Data
@TableName("device_status_timeline")
public class DeviceWarnLogEntity{

    @TableId("id")
    @JsonProperty("id")
    private String id;

    @TableField("DEVICE_ID")
    @JsonProperty("deviceId")
    private String deviceId;

    @TableField("EXEC_CODE")
    @JsonProperty("exceCode")
    private String exceCode;

    @TableField("EXEC_CONTENT")
    @JsonProperty("exceContent")
    private String exceContent;

}
