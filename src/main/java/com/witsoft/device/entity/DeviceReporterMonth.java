package com.witsoft.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("device_month_reporter")
public class DeviceReporterMonth {

    @TableId(type = IdType.AUTO)
    private Integer id;
    //设备月度时间稼动率
    private Double timeGrainMoveRate;
    //设备月度稼动率
    private Double performanceGrainMoveRate;

    @TableField(value = "month_running_time")
    private Long sumRunningTime;
    @TableField(value = "month_opening_time")
    private Long sumOpeningTime;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private String deviceId;

    public DeviceReporterMonth() {
    }


    public DeviceReporterMonth(Long sumRunningTime, Long sumOpeningTime, Date createTime, String deviceId) {
        this.sumRunningTime = sumRunningTime;
        this.sumOpeningTime = sumOpeningTime;
        this.createTime = createTime;
        this.deviceId = deviceId;
    }
}
