package com.witsoft.device.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@TableName("device_status_timeline")
public class DeviceStatusTimeLineEntity{

    @TableId("id")
    @JsonProperty("id")
    private String id;

    @TableField("DEVICE_ID")
    @JsonProperty("deviceId")
    private String deviceId;


    /**
     * @desc 设备当前状态:0：开机 1：正常 2：空闲 3：故障 4：关机
     */
    @TableField("STATUS")
    @JsonProperty("status")
    private String status;

    /**
     * @desc 设备状态时序图的起始时间点
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("START_TIME")
    @JsonProperty("startTime")
    private Date startTime;

    /**
     * @desc 设备状态时序图的结束时间点
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("END_TIME")
    @JsonProperty("endTime")
    private Date endTime;

    /**
     * @desc 状态事件时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("EVENT_TIME")
    @JsonProperty("eventTime")
    private Date eventTime;


    /**
     * @desc 开机时间消耗，运行时间消耗 格式：s秒
     */
    @TableField("total_spent")
    @JsonProperty("totalSpent")
    private Long totalSpent;

}
