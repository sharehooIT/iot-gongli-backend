package com.witsoft.device.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
@TableName("device")
public class DeviceEntity{

    @TableId("id")
    @JsonProperty("id")
    private String id;

    @TableField("TYPE")
    @JsonProperty("type")
    private String type;

    @TableField("ALIAS")
    @JsonProperty("alias")
    private String alias;

    @TableField("NAME")
    @JsonProperty("name")
    private String name;

    @TableField("MODEL_NUMBER")
    @JsonProperty("modelNumber")
    private String modelNumber;

    @TableField("SERIAL_NUMBER")
    @JsonProperty("serialNumber")
    private String serialNumber;

    @TableField("LOCATION")
    @JsonProperty("location")
    private String location;

    @TableField("KPI_CALA_PERIOD")
    @JsonProperty("kpiCalaPeriod")
    private Long kpiCalaPeriod;

    @TableField("DEVICE_METER")
    @JsonProperty("deviceMeter")
    private double deviceMeter;

    @TableField("PLAN_START_TIME")
    @JsonProperty("planStartTime")
    private Long planStartTime;

    @TableField("PLAN_END_TIME")
    @JsonProperty("planEndTime")
    private Long planEndTime;

    //初始化值为0，防止指标计算空指针问题
    @TableField("GOOD_COUNT")
    @JsonProperty("goodCount")
    private Long goodCount;

    //初始化值为0，防止指标计算空指针问题
    @TableField("BAD_COUNT")
    @JsonProperty("badCount")
    private Long badCount;

    //初始化值为0，防止指标计算空指针问题
    @TableField("TOTAL_COUNT")
    @JsonProperty("totalCount")
    private Long totalCount;

    //初始化值为0，防止指标计算空指针问题
    @TableField("IDEAL_RUN_RATE")
    @JsonProperty("idealRunRate")
    private Long idealRunRate;

    @TableField("SORT")
    @JsonProperty("sort")
    private int sort;

    @TableField("STATUS")
    @JsonProperty("status")
    //0：开机 1：正常 2：待机 3：故障 4：关机
    private String status;

    @TableField("CREATED_TIME")
    @JsonProperty("createdTime")
    private Long createdTime;


}
