package com.witsoft.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("device_day_reporter")
public class DeviceReporterDay {

    @TableId(type = IdType.AUTO)
    private Integer id;

    //设备每天的时间稼动率
    private Double timeGrainMoveRate;
    //设备每天的性能稼动率
    private Double performanceGrainMoveRate;


    //设备每天的运行时长
    @TableField(value = "month_running_time")
    private Long sumRunningTime;
    //设备每天的开机时长
    @TableField(value = "month_opening_time")
    private Long sumOpeningTime;

    private Date countDate;

    private String deviceId;

    //feature(2022.01.11): 生产产品数
    private Long numberOfProducts;


    public DeviceReporterDay() {
    }

    public DeviceReporterDay(Long sumRunningTime, Long sumOpeningTime, Date countDate, String deviceId) {
        this.sumRunningTime = sumRunningTime;
        this.sumOpeningTime = sumOpeningTime;
        this.countDate = countDate;
        this.deviceId = deviceId;
    }
}
