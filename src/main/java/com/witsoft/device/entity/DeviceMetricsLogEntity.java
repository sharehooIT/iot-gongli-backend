package com.witsoft.device.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@TableName("device_metrics_log")
public class DeviceMetricsLogEntity{

    @TableId("id")
    @JsonProperty("id")
    private String   id;

    @TableField("DEVICE_ID")
    @JsonProperty("deviceId")
    private String   deviceId;

    @TableField("AVAILABILITY")
    @JsonProperty("availability")
    private String availability;

    @TableField("PERFORMANCE")
    @JsonProperty("performance")
    private String performance;

    @TableField("QUALITY")
    @JsonProperty("quality")
    private String quality;

    @TableField("OEE")
    @JsonProperty("oee")
    private String oee;

    @TableField("CREATE_TIME")
    @JsonProperty("createTime")
    private Date   createTime;

    @TableField("GOOD_COUNT")
    @JsonProperty("goodCount")
    private Long   goodCount;

    @TableField("BAD_COUNT")
    @JsonProperty("badCount")
    private Long   badCount;

    @TableField("TOTAL_COUNT")
    @JsonProperty("totalCont")
    private Long   totalCont;

    @TableField("TYPE")
    @JsonProperty("type")
    private String type;

}
