package com.witsoft.rabbitMq;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.witsoft.device.dao.DeviceStatusTimeLineDao;
import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.entity.DeviceStatusTimeLineEntity;
import com.witsoft.device.service.DeviceService;
import com.witsoft.device.service.DeviceStatusTimeLineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * @Titile: 时序数据
 * @Description:
 * @Author: liyang
 * @Date: 2021/4/20 10:07
 */
@Slf4j
@Component
@RabbitListener(queues = "reportStatus")
public class StatusLineConsumer{

    @Autowired
    private DeviceService               deviceService;

    @Resource
    private DeviceStatusTimeLineDao     deviceStatusTimeLineDao;

    @Resource
    private DeviceStatusTimeLineService deviceStatusTimeLineService;

    @RabbitHandler
    public void process(JSONObject data) {
        log.info("Receiver reportStatus: " + data);
        try{
            DeviceEntity device = deviceService.getDeviceByName(data.get("DeviceName").toString());
            if(device != null &&  data.get("Running") != null && !StringUtils.isEmpty(data.get("Running").toString())){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                DeviceStatusTimeLineEntity newEntity = new DeviceStatusTimeLineEntity();
                newEntity.setId(UUID.randomUUID().toString());
                newEntity.setDeviceId(device.getId());
                newEntity.setStatus(data.get("Running").toString());
                newEntity.setStartTime(sdf.parse(data.get("CreateDate").toString()));
                newEntity.setEventTime(sdf.parse(data.get("CreateDate").toString()));
                //设备状态时序图处理
                //根据设备Id获取当日时间顺拍最后一条数据，比较状态和时间，如果没有数据，则当前上报的数据为当天第一条数据
                DeviceStatusTimeLineEntity lastOne = deviceStatusTimeLineDao.getLastOneInfoByDeviceId(device.getId());
                if(lastOne != null){
                    //如果状态未发生变化，则不继续处理
                    if(lastOne.getStatus().equals(data.get("Running").toString())) return;
                    //设备断开连接状态默认为关机
                    if ("0".equals(data.get("Machines Online").toString())){
                        lastOne.setEndTime(sdf.parse(data.get("CreateDate").toString()));
                        deviceStatusTimeLineService.saveOrUpdate(lastOne);
                    }
                    else {
                        //正常时序
                        if (sdf.parse(data.get("CreateDate").toString()).compareTo(lastOne.getEventTime()) > 0){
                            //最后一条记录补充结束时间
                            lastOne.setEndTime(sdf.parse(data.get("CreateDate").toString()));
                            deviceStatusTimeLineService.saveOrUpdate(lastOne);
                        }
                        else {
                            //乱序数据(当前状态变化数据为之前发送但延迟到的变化数据),和当前设备当日的倒数第二条数据置换结束时间
                            DeviceStatusTimeLineEntity lastTwoStatusRecord = deviceStatusTimeLineDao.getLastTwoStatusRecordByDeviceId(device.getId());
                            if (lastTwoStatusRecord != null){
                                //修改倒数第二条数据的结束时间为当前传入时序的事件时间
                                lastTwoStatusRecord.setEndTime(sdf.parse(data.get("CreateDate").toString()));
                                deviceStatusTimeLineService.saveOrUpdate(lastTwoStatusRecord);
                            }
                            //最后一条记录的结束时间置空
                            lastOne.setEndTime(null);
                            deviceStatusTimeLineService.saveOrUpdate(lastOne);
                            newEntity.setEndTime(lastOne.getEventTime());
                        }
                    }
                }
                deviceStatusTimeLineService.save(newEntity);
            }
        }catch(Exception e){
            log.error(e.getMessage());
        }
    }
}
