package com.witsoft.rabbitMq;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.witsoft.device.dao.DeviceStatusTimeLineDao;
import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
@RabbitListener(queues = "oeeParams")
public class RabbitOeeConsumer{

    @Autowired
    private DeviceService  deviceService;

    @Resource
    private DeviceStatusTimeLineDao deviceStatusTimeLineDao;

    @RabbitHandler
    public void process(JSONObject data) {
        log.info("Receiver calcOee: " + data);
        DeviceEntity device = deviceService.getDeviceByName(data.get("DeviceName").toString());
        if(device != null &&  data.get("Running") != null && !StringUtils.isEmpty(data.get("Running").toString())){
            //设备状态，遥测数据 处理
            if(null != data.get("Running")){
                device.setStatus(data.get("Running").toString());
            }
            if(null != data.get("Good Quantity")){
                device.setGoodCount(Long.parseLong(data.get("Good Quantity").toString()));
            }
            if(null != data.get("Bad Quantity")){
                device.setBadCount(Long.parseLong(data.get("Bad Quantity").toString()));
            }
            if(null != data.get("Good Quantity") || null != data.get("Bad Quantity")){
                device.setTotalCount(device.getGoodCount()+device.getBadCount());
            }
            deviceService.saveOrUpdate(device);
        }
    }
}
