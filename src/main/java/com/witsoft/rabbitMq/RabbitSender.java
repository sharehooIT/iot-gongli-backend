package com.witsoft.rabbitMq;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitSender{
    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void reportStatus(JSONObject data){
        try{
            String sendMsg = "reportStatus " + new Date();
            System.out.println("reportStatus Send: " + data);
            this.rabbitTemplate.convertAndSend("reportStatus", data);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void reportOeeParam(JSONObject data) {
        try{
            String sendMsg = "oeeParams " + new Date();
            System.out.println("oeeParams Send: " + data);
            this.rabbitTemplate.convertAndSend("oeeParams", data);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
