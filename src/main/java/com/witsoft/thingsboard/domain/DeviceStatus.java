package com.witsoft.thingsboard.domain;


import com.witsoft.device.enums.MachineStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DeviceStatus implements Serializable {

    private String deviceName;

    //1:运行中 0：待机（空闲）
    private String running;

    //给一个初始化值，防止计算的时候出现空指针问题
    private Integer goodQuantity = 0;
    private Integer badQuantity = 0;

    private Date createDate;

    //设备断开连接状态默认为关机 0: 关机 1：开机, 优先级比running高，只有在machinesOnline值等于1的情况下，running状态才有效
    private String machinesOnline;


    public Integer getGoodQuantity() {
        if(goodQuantity == null){
            return 0;
        }
        return goodQuantity;
    }

    public Integer getBadQuantity() {
        if(badQuantity == null){
            return 0;
        }
        return badQuantity;
    }

    /**
     * @desc 统一处理状态码
     * @return
     */
    public MachineStatusEnum getStatus(){

        //！！设备开关状态优先级高于运行状态，关机的情况下不处理运行状态
        if(null != machinesOnline){
            if(machinesOnline.equals(MachineStatusEnum.TURNING.getValue()))
                return MachineStatusEnum.TURNING;

            return MachineStatusEnum.STOPPING;
        }

        if(null != running){
            if(running.equals(MachineStatusEnum.RUNNING.getValue()))
                return MachineStatusEnum.RUNNING;

            return MachineStatusEnum.WAITING;
        }

        return null;
    }


    public MachineStatusEnum getRunningStatus(){

        //！！设备开关状态优先级高于运行状态，关机的情况下不处理运行状态
        if(null != running){
            if(running.equals(MachineStatusEnum.RUNNING.getValue()))
                return MachineStatusEnum.RUNNING;

            return MachineStatusEnum.WAITING;
        }

        //如果是关机，则离线
        if(null != machinesOnline){
            if(machinesOnline.equals(MachineStatusEnum.STOPPING.getValue()))
                return MachineStatusEnum.STOPPING;
        }

        return null;
    }

}
