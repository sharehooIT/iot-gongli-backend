package com.witsoft.manager;


import com.witsoft.thingsboard.domain.DeviceStatus;
import org.springframework.stereotype.Component;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;


/**
 * @desc 队列管理类
 */
@Component
public class QueueManager {

    /**
     * @desc 事件缓冲队列，当线程池满了，则将消息缓存到此队列中
     */
    private Queue<Object> msgQueue = new LinkedBlockingDeque<Object>();


    public void offerMsg(DeviceStatus event){
        msgQueue.offer(event);
    }


    public Boolean msgQueueIsEmpty(){
        return msgQueue.isEmpty();
    }


    public Object pool(){
        return msgQueue.poll();
    }


    /**
     * @desc 获取消息缓冲队列
     * @return
     */
    public Queue<Object> getMsgQueue() {
        return msgQueue;
    }
}
