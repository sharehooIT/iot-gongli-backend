package com.witsoft.manager;


import com.alibaba.fastjson.JSONObject;
import com.witsoft.device.utils.Result;
import com.witsoft.thingsboard.domain.DeviceStatus;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

/**
 * @desc 多线程消息处理事件
 */
@Data
@Slf4j
public class MessageThreadHandler implements Runnable{


    private DeviceStatus deviceStatus;
    private RestTemplate restTemplate;

    private String host;
    private String mesSendApi;
    private String sign;

    public MessageThreadHandler(DeviceStatus deviceStatus, RestTemplate restTemplate, String host, String mesSendApi, String sign) {
        this.deviceStatus = deviceStatus;
        this.restTemplate = restTemplate;
        this.host = host;
        this.mesSendApi = mesSendApi;
        this.sign = sign;
    }

    @Override
    public void run() {
        //log.info("向mes发送消息...");
        try{
            //防止设备数据为null,只发送数据变化的事件消息
            if(ObjectUtils.isEmpty(deviceStatus.getMachinesOnline()) && ObjectUtils.isEmpty(deviceStatus.getRunning())){
                String url = host + mesSendApi + "?deviceName="+deviceStatus.getDeviceName() +"&goodQuantity=" + deviceStatus.getGoodQuantity() +
                        "&badQuantity="+ deviceStatus.getBadQuantity() + "&sign=" + sign +"&createDate="+deviceStatus.getCreateDate().getTime();

                restTemplate.getForObject(url, Result.class);
            }
            //log.info("#### mes 调用结果：{}", JSONObject.toJSONString(result));
        }catch (Exception e){
            log.info("#### mes 调用失败原因：{}", e.getMessage());
        }

    }



}
