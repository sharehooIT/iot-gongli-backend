package com.witsoft.service;


import com.witsoft.device.entity.DeviceEntity;
import com.witsoft.device.entity.DeviceReporterMonth;
import com.witsoft.device.service.DeviceReporterService;
import com.witsoft.device.service.DeviceService;
import com.witsoft.device.service.DeviceStatusTimeLineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @desc 月度报表测试类
 * @author miki
 * @date 2021.11.03
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReporterService {

    @Resource
    private DeviceStatusTimeLineService deviceStatusTimeLineService;

    @Resource
    private DeviceReporterService reporterService;

    @Resource
    private DeviceService deviceService;

    @Test
    public void testReport(){
        List<DeviceEntity> deviceList = deviceService.getAllList();
        List<DeviceReporterMonth> reporterMonths = new ArrayList<>();

        if(!CollectionUtils.isEmpty(deviceList)) {
            for (DeviceEntity deviceEntity : deviceList) {
                Long sumRunningTimeMonth = deviceStatusTimeLineService.getSumRunningTimeLastMonth(deviceEntity.getId());
                Long sumOPenningMonth = deviceStatusTimeLineService.getSumOpeningLastMonth(deviceEntity.getId());

                DeviceReporterMonth deviceReporterMonth = new DeviceReporterMonth(sumRunningTimeMonth, sumOPenningMonth, new Date(), deviceEntity.getId());
                reporterMonths.add(deviceReporterMonth);
            }

            reporterService.saveBatch(reporterMonths);
        }
    }
}
