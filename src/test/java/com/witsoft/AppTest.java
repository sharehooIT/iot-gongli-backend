package com.witsoft;

import static org.junit.Assert.assertTrue;

import com.alibaba.fastjson.JSONObject;
import com.witsoft.thingsboard.domain.DeviceStatus;
import org.junit.Test;

import java.text.DecimalFormat;
import java.text.MessageFormat;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }


    public static void main(String[] args) {
        String dd = "{\"DeviceName\":\"S27\",\"goodQuantity\":650329,\"createDate\":\"2021-12-20 00:07:58\"}";

        DeviceStatus deviceStatus = JSONObject.parseObject(dd, DeviceStatus.class);

        String parttern = "?deviceName={0}&sign={1}";

        String format = MessageFormat.format(parttern, "11", "44");
        System.out.println(format);

        DecimalFormat df = new DecimalFormat("#");
        System.out.println("e3eeeeee "+ df.format(0.0187));
    }
}
