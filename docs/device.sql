/*
 Navicat Premium Data Transfer

 Source Server         : wit-pg
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : 192.168.1.230:5432
 Source Catalog        : iot_sp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 18/11/2021 09:04:23
*/


-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS "public"."device";
CREATE TABLE "public"."device" (
  "id" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "created_time" int8,
  "type" varchar(255) COLLATE "pg_catalog"."default",
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "model_number" varchar(50) COLLATE "pg_catalog"."default",
  "serial_number" varchar(50) COLLATE "pg_catalog"."default",
  "location" varchar(255) COLLATE "pg_catalog"."default",
  "kpi_cala_period" int8,
  "plan_start_time" int8,
  "plan_end_time" int8,
  "good_count" int8 DEFAULT 0,
  "total_count" int8 DEFAULT 0,
  "ideal_run_rate" int8 DEFAULT 0,
  "sort" int4,
  "status" varchar(10) COLLATE "pg_catalog"."default" DEFAULT 2,
  "alias" varchar(255) COLLATE "pg_catalog"."default",
  "bad_count" int8 DEFAULT 0,
  "device_meter" numeric(8,1)
)
;
COMMENT ON COLUMN "public"."device"."id" IS 'ID';
COMMENT ON COLUMN "public"."device"."created_time" IS '创建时间';
COMMENT ON COLUMN "public"."device"."type" IS '设备类型：设备（default），厂级（factory_line）,车间级（workshop_line）,';
COMMENT ON COLUMN "public"."device"."name" IS '设备名称';
COMMENT ON COLUMN "public"."device"."model_number" IS '设备型号';
COMMENT ON COLUMN "public"."device"."serial_number" IS '序列号';
COMMENT ON COLUMN "public"."device"."location" IS '位置';
COMMENT ON COLUMN "public"."device"."kpi_cala_period" IS 'KPI更新周期（ms）';
COMMENT ON COLUMN "public"."device"."plan_start_time" IS '计划开始时间';
COMMENT ON COLUMN "public"."device"."plan_end_time" IS '计划结束时间';
COMMENT ON COLUMN "public"."device"."good_count" IS '合格产品数量';
COMMENT ON COLUMN "public"."device"."total_count" IS '总的产品数量';
COMMENT ON COLUMN "public"."device"."ideal_run_rate" IS '理想运行效率';
COMMENT ON COLUMN "public"."device"."sort" IS '排序字段';
COMMENT ON COLUMN "public"."device"."status" IS '设备当前状态:0：故障 1：正常  2：空闲 3：开机 4：停机';
COMMENT ON COLUMN "public"."device"."alias" IS '别名';
COMMENT ON COLUMN "public"."device"."bad_count" IS '不合格品数量';
COMMENT ON COLUMN "public"."device"."device_meter" IS '节拍时间(秒)';

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO "public"."device" VALUES ('1', 1635846953001, NULL, 'S10', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('10', 1635846953002, NULL, 'S09', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('11', 1635846953003, NULL, 'S08', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('12', 1635846953004, NULL, 'S07', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('13', 1635846953005, 'boda_device_profile', 'S06', '', '1111', NULL, 60, 1634688000000, NULL, 0, 0, 0, 1, '2', '', 0, 1.0);
INSERT INTO "public"."device" VALUES ('14', 1635846953006, 'boda_device_profile', 'S22', NULL, '2222', NULL, 60, 1634688000000, NULL, 0, 0, 0, 4, '0', NULL, 0, 1.0);
INSERT INTO "public"."device" VALUES ('15', 1635846953007, 'boda_device_profile', 'S25', '', '4444', NULL, 60, 1634688000000, 1634688001100, 0, 0, 0, 2, '3', '', 0, 1.0);
INSERT INTO "public"."device" VALUES ('16', 1635846953008, 'boda_device_profile', 'S30', 'WO01', '44456', NULL, 60, 1634688000000, 1634688003150, 0, 0, 86, 5, '1', NULL, 0, 2.0);
INSERT INTO "public"."device" VALUES ('17', 1635846953009, 'boda_device_profile', 'S34', 'WO03', '6666', NULL, 60, 1634688000000, NULL, 0, 0, 60, 6, '3', NULL, 0, 1.0);
INSERT INTO "public"."device" VALUES ('18', 1635846953010, 'boda_device_profile', 'S35', 'WO04', '7777', NULL, 60, 1634688000000, NULL, 0, 0, 60, 6, '0', NULL, 0, 1.0);
INSERT INTO "public"."device" VALUES ('19', 1635846953011, 'boda_device_profile', 'S18', 'WO05', '8888', NULL, 60, 1634688000000, NULL, 0, 0, 80, 6, '2', NULL, 0, 1.0);
INSERT INTO "public"."device" VALUES ('2', 1635846953012, 'boda_device_profile', 'S29', '', '789899', NULL, 60, 1634688000000, NULL, 0, 0, 0, 3, '1', '', 0, 1.0);
INSERT INTO "public"."device" VALUES ('3', 1635846953014, NULL, 'S11', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('3', 1635846953014, NULL, 'S11', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('5', 1635846953015, NULL, 'S32', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('6', 1635846953016, NULL, 'S27', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('7', 1635846953017, NULL, 'S33', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('8', 1635846953018, NULL, 'S23', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
INSERT INTO "public"."device" VALUES ('9', 1635846953019, NULL, 'S19', NULL, '10000', NULL, NULL, NULL, NULL, 0, 0, 0, 0, '1', NULL, 0, 0.0);
