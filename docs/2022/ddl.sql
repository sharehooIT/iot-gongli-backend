ALTER TABLE "public"."device_day_reporter"
  ADD COLUMN "number_of_products" int8 DEFAULT 0;

COMMENT ON COLUMN "public"."device_day_reporter"."number_of_products" IS '当日生产总数';